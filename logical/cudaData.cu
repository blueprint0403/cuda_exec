#include <cassert>
#include <iostream>

using namespace std;

////////////////////////////

template <class Type>
cudaData<Type>::cudaData(size_t len_host, size_t len_dev)
{
	if(len_dev == 0) len_dev = len_host;

	this->len_host = len_host;
	this->len_dev  = len_dev;

	host = new Type[ len_host ];	

	cudaMalloc((void**) &dev, len_dev * sizeof(Type));
}

template <class Type>
cudaData<Type>::~cudaData()
{
	delete [] host;
	cudaFree(dev);
}

////////////////////////////

template <class Type>
size_t cudaData<Type>::get_len() const
{
	assert( len_host == len_dev );

	return len_host;
}

template <class Type>
size_t cudaData<Type>::get_len_host() const
{
	return len_host;
}

template <class Type>
size_t cudaData<Type>::get_len_dev() const
{
	return len_dev;
}

template <class Type>
size_t cudaData<Type>::get_size() const
{
	assert( len_host == len_dev );

	return len_host * sizeof(Type);
}

template <class Type>
size_t cudaData<Type>::get_size_host() const
{
	return len_host * sizeof(Type);
}

template <class Type>
size_t cudaData<Type>::get_size_dev() const
{
	return len_dev * sizeof(Type);
}

template <class Type>
void cudaData<Type>::dump() const
{
	cout << "host array: " << get_len_host() << " size-" << sizeof(Type) << " elements" << endl;
	cout << "device array: " << get_len_dev() << " size-" << sizeof(Type) <<  " elements" << endl;
}

////////////////////////////

template <class Type>
void cudaData<Type>::clear()
{
	memset(host, 0, this->get_size_host());
}

template <class Type>
void cudaData<Type>::write()
{
	cudaMemcpy(dev, host, this->get_size(), cudaMemcpyHostToDevice);
}

template <class Type>
void cudaData<Type>::read()
{
	assert( len_host == len_dev );

	cudaMemcpy(host, dev, this->get_size(), cudaMemcpyDeviceToHost);
}

/*
template <class Type>
void cudaData<Type>::write(size_t off_src, size_t size, size_t off_des)
{
	if(size == 0) size = (sz_host <= sz_dev) ? sz_host : sz_dev;

	cudaMemcpy(&dev[off_src], &host[off_des], size, cudaMemcpyHostToDevice);
}

template <class Type>
void cudaData<Type>::read(size_t off_src, size_t size, size_t off_des)
{
	if(size == 0) size = (sz_host <= sz_dev) ? sz_host : sz_dev;

	cudaMemcpy(&host[off_src], &dev[off_des], size, cudaMemcpyDeviceToHost);
}
*/

