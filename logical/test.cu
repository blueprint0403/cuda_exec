#include <stdint.h>
#include <cstdio>
#include <cstdlib>    
#include <ctime>

#include "cudaData.h"


///

#define measure_start(start, stop, time) cudaEvent_t start, stop;\
                                         float time;\
                                         cudaEventCreate(&start);\
                                         cudaEventCreate(&stop);\
                                         cudaEventRecord(start, 0)

#define measure_end(start, end, time) cudaEventRecord(stop, 0);\
                                      cudaEventSynchronize(stop);\
                                      cudaEventElapsedTime(&time, start, stop);\
                                      cudaEventDestroy(start);\
                                      cudaEventDestroy(stop)

#define NUM (12 * 32 * 1024)
#define BLOCKDIM 1024

int64_t get_gpu_cycles(int seconds=1)
{
    // Get device frequency in Hz
    int64_t Hz;
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    Hz = int64_t(prop.clockRate) * 1000;
 
    // Calculate number of cycles to wait
    int64_t num_cycles;
    num_cycles = seconds * Hz;
 
    return num_cycles;
}

__constant__ uint32_t iters[1];

#include "kernel.cu"

typedef uint32_t Type;

int main()
{
	cout << "cycles:" << get_gpu_cycles() << endl;

	///

	uint32_t iters_host = 100000;
	cudaMemcpyToSymbol(iters, &iters_host, 4);

	cudaData<Type> ret(NUM);
	cudaData<Type> data1(NUM * 9);
	cudaData<Type> data2(NUM * 9);

	srand (time(NULL));
	for (unsigned i = 0; i < data1.get_len_host(); i++)
	{
		data1.host[i] = rand();
		data2.host[i] = rand();
	}

	data1.write();
	data2.write();

	///

	measure_start(start, stop, time);

	kernel<Type><<<NUM / BLOCKDIM, BLOCKDIM>>>(ret.dev, data1.dev, data2.dev);

	measure_end(start, stop, time);
	cout << "Kernel time: " << time << " ms" << endl;

	///

	return 0;
}

