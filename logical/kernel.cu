//#include "kernel.h"

#define NTHREADS (blockDim.x * gridDim.x)

template<class T>
__global__ void kernel(T * result,
                       T * data1, 
                       T * data2)
{
	uint32_t idx = blockDim.x * blockIdx.x + threadIdx.x;
	uint32_t local_iters = iters[0];

	T a0 = data1[idx + 0 * NTHREADS];
	T a1 = data1[idx + 1 * NTHREADS];
	T a2 = data1[idx + 2 * NTHREADS];
	T a3 = data1[idx + 3 * NTHREADS];
	T a4 = data1[idx + 4 * NTHREADS];
	T a5 = data1[idx + 5 * NTHREADS];
	T a6 = data1[idx + 6 * NTHREADS];
	T a7 = data1[idx + 7 * NTHREADS];
	T a8 = data1[idx + 8 * NTHREADS];

	T b0 = data2[idx + 0 * NTHREADS];
	T b1 = data2[idx + 1 * NTHREADS];
	T b2 = data2[idx + 2 * NTHREADS];
	T b3 = data2[idx + 3 * NTHREADS];
	T b4 = data2[idx + 4 * NTHREADS];
	T b5 = data2[idx + 5 * NTHREADS];
	T b6 = data2[idx + 6 * NTHREADS];
	T b7 = data2[idx + 7 * NTHREADS];
	T b8 = data2[idx + 8 * NTHREADS];

	for (unsigned i = 0; i < local_iters; i++)
	{
		a7 += a8;
		b7 += b8;
		a6 += a7;
		b6 += b7;
		a5 += a6;
		b5 += b6;
		a4 += a5;
		b4 += b5;
		a3 += a4;
		b3 += b4;
		a2 += a3;
		b2 += b3;
		a1 += a2;
		b1 += b2;
		a0 += a1;
		b0 += b1;

		a1 ^= a0;
		b1 ^= b0;
		a2 ^= a1;
		b2 ^= b1;
		a3 ^= a2;
		b3 ^= b2;
		a4 ^= a3;
		b4 ^= b3;
		a5 ^= a4;
		b5 ^= b4;
		a6 ^= a5;
		b6 ^= b5;
		a7 ^= a6;
		b7 ^= b6;
		a8 ^= a7;
		b8 ^= b7;

		a7 -= a8;
		b7 -= b8;
		a6 -= a7;
		b6 -= b7;
		a5 -= a6;
		b5 -= b6;
		a4 -= a5;
		b4 -= b5;
		a3 -= a4;
		b3 -= b4;
		a2 -= a3;
		b2 -= b3;
		a1 -= a2;
		b1 -= b2;
		a0 -= a1;
		b0 -= b1;

		a1 &= a0;
		b1 &= b0;
		a2 &= a1;
		b2 &= b1;
		a3 &= a2;
		b3 &= b2;
		a4 &= a3;
		b4 &= b3;
		a5 &= a4;
		b5 &= b4;
		a6 &= a5;
		b6 &= b5;
		a7 &= a6;
		b7 &= b6;
		a8 &= a7;
		b8 &= b7;
	}

	a0 += a1;
	a0 += a2;
	a0 += a3;
	a0 += a4;
	a0 += a5;
	a0 += a6;
	a0 += a7;
	a0 += a8;
	a0 += b0;
	a0 += b1;
	a0 += b2;
	a0 += b3;
	a0 += b4;
	a0 += b5;
	a0 += b6;
	a0 += b7;
	a0 += b8;

	result[ idx ] = a0;
}

