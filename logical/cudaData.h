#ifndef CUDA_DATA_H
#define CUDA_DATA_H	

#include <cuda.h>

template <class Type>
class cudaData
{
public:

	cudaData(size_t len_host, size_t len_dev=0);
	~cudaData();

	size_t get_len() const;
	size_t get_len_host() const;
	size_t get_len_dev() const;
	size_t get_size() const;
	size_t get_size_host() const;
	size_t get_size_dev() const;
	void dump() const;

	void clear();
	void write();		
	void read();

//	void write(size_t off_src=0, size_t len=0, size_t off_des=0);		
//	void read(size_t off_src=0, size_t len=0, size_t off_des=0);

	Type * host;
	Type * dev;

private:

	size_t len_host;
	size_t len_dev;
};

#include "cudaData.cu"

#endif

