#ifndef KERNEL_H
#define KERNEL_H

#include <stdint.h>

__global__ void kernel(float *, float *, float *);

#endif

